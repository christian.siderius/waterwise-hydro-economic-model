# WaterWise hydro-economic model

‘WaterWise’ is a hydro-economic model with the ability to generate spatially varied patterns of measures that make best use of the available land and water resources. WaterWise is based on Linear Programming, with multi-objective functionality.

**Copyright**
Copyright (C) 2021-2022 Wageningen University & Research. The WaterWise model is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.

Contacts: paul@vanwater.eu; christian.siderius@wur.nl
